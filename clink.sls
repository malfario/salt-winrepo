clink:
  0.4.2:
    installer: 'https://github.com/mridgers/clink/releases/download/0.4.2/clink_0.4.2_setup.exe'
    full_name: 'Clink v0.4.2'
    install_flags: '/S'
    uninstaller: '%PROGRAMFILES%\clink\0.4.2\clink_uninstall_0.4.2.exe'
    uninstall_flags: '/S'
