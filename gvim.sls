gvim:
  7.4.423:
    installer: 'http://garr.dl.sourceforge.net/project/cream/Vim/7.4.423/gvim-7-4-423.exe'
    full_name: 'Vim 7.4.423'
    reboot: False
    install_flags: '/S /CONTEXTMENU 1'
    uninstaller: '%PROGRAMFILES%\Vim\vim74\uninstall.exe'
    uninstall_flags: '/S'
