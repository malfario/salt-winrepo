jnemo:
  0.11.5.0:
    installer: 'http://nemo.notin.net/updates/jnemo-0.11.5-20080109.exe'
    full_name: 'jNemo'
    install_flags: '/S'
    uninstaller: '%PROGRAMFILES%\jNemo\uninstall.exe'
    uninstall_flags: '/S'
  0.12.0:
    installer: 'http://nemo.notin.net/jnemo-latest.exe'
    full_name: 'jNemo'
    install_flags: '/S'
    uninstaller: '%PROGRAMFILES%\jNemo\uninstall.exe'
    uninstall_flags: '/S'
