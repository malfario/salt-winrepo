putty:
  0.63:
    installer: 'http://the.earth.li/~sgtatham/putty/latest/x86/putty-0.63-installer.exe'
    full_name: 'PuTTY 0.63'
    reboot: False
    install_flags: '/SP- /VERYSILENT'
    uninstaller: '%PROGRAMFILES%\PuTTY\unins000.exe'
    uninstall_flags: '/VERYSILENT'
