rapidee:
  latest:
    installer: 'http://www.rapidee.com/download/RapidEE_setup.exe'
    full_name: 'Rapid Environment Editor version 8.0.0.923'
    install_flags: '/VERYSILENT'
    uninstaller: '%PROGRAMFILES%\Rapid Environment Editor\unins000.exe'
    uninstall_flags: '/VERYSILENT'
