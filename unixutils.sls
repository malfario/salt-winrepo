unixutils:
  0.1:
    installer: 'http://notin.net/files/UnixUtils-0.1.exe'
    full_name:  'UnixUtils 0.1'
    reboot: False
    install_flags: '/S'
    uninstaller: '%PROGRAMFILES%\UnixUtils\uninstall.exe'
    uninstall_flags: '/S'
